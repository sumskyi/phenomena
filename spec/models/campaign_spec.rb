require 'spec_helper'

describe Campaign do
  context 'store serialized countries and languages' do
    subject(:campaign) { create(:campaign) }

    let(:some_value) do
      [{:a => 1, :b => 2}, {:a => 2, :b => 1}]
    end

    it 'encode' do
      campaign.countries_languages = some_value
      campaign.save
      campaign.reload
      campaign.countries_languages.should == some_value
    end
  end
end
