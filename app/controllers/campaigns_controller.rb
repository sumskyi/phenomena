class CampaignsController < ApplicationController
  respond_to :json, :html

  expose(:campaign)
  expose(:campaigns)

  def index
  end

  def show
    respond_with campaign
  end

  def new
    respond_with campaign
  end

  def edit
    respond_with campaign
  end

  def create
    if campaign.save
      respond_with campaign, status: :created, location: campaign
    else
      render json: campaign.errors, status: :unprocessable_entity
    end
  end

  def update
    if campaign.update_attributes(params[:campaign])
      head :no_content
    else
      render json: campaign.errors, status: :unprocessable_entity
    end
  end

  def destroy
    campaign.destroy
    head :no_content
  end
end
