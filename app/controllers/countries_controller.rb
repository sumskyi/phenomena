class CountriesController < ApplicationController
  respond_to :json

  expose(:countries) { Country.order('name ASC') }

  def index
    respond_with countries
  end
end
