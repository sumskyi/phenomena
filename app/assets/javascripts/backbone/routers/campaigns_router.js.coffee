class PhenomenaTestTask.Routers.CampaignsRouter extends Backbone.Router
  initialize: (options) ->
    @campaigns = new PhenomenaTestTask.Collections.CampaignsCollection()
    @campaigns.reset options.campaigns

  routes:
    "new"      : "newCampaign"
    "index"    : "index"
    ":id/edit" : "edit"
    ":id"      : "show"
    ".*"        : "index"

  newCampaign: ->
    @view = new PhenomenaTestTask.Views.Campaigns.NewView(collection: @campaigns)
    $("#campaigns").html(@view.render().el)

  index: ->
    @view = new PhenomenaTestTask.Views.Campaigns.IndexView(campaigns: @campaigns)
    $("#campaigns").html(@view.render().el)

  show: (id) ->
    campaign = @campaigns.get(id)

    @view = new PhenomenaTestTask.Views.Campaigns.ShowView(model: campaign)
    $("#campaigns").html(@view.render().el)

  edit: (id) ->
    campaign = @campaigns.get(id)

    @view = new PhenomenaTestTask.Views.Campaigns.EditView(model: campaign)
    $("#campaigns").html(@view.render().el)
