#= require 'backbone/views/campaigns/persistence_view'

PhenomenaTestTask.Views.Campaigns ||= {}

class PhenomenaTestTask.Views.Campaigns.NewView extends PhenomenaTestTask.Views.Campaigns.PersistenceView
  template: JST["backbone/templates/campaigns/new"]

  events:
    "submit #new-campaign": "save"
    "click #add-country": "add_row"

  constructor: (options) ->
    super(options)
    @model = new @collection.model()

    @model.on("change:errors", @renderErrors, this)


  save: (e) ->
    e.preventDefault()
    e.stopPropagation()

    @fill_inputs()

    @model.unset("errors")

    @collection.create(@model.toJSON(),
      success: (campaign) =>
        @model = campaign
        window.location.hash = "/#{@model.id}"

      error: (campaign, jqXHR) =>
        @model.set({errors: $.parseJSON(jqXHR.responseText)})
    )



  add_row: ->
    view = new PhenomenaTestTask.Views.Campaigns.CountryRowView({model: campaign})
    table = this.$el.find('#countries')
    table.prepend(view.render().el)
    return

  add_rows: ->
    return
