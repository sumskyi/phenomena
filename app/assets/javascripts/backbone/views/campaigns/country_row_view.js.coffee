PhenomenaTestTask.Views.Campaigns ||= {}

class PhenomenaTestTask.Views.Campaigns.CountryRowView extends Backbone.View
  template: JST["backbone/templates/campaigns/country_row"]

  tagName: "tr"
  className: "country-row"

  events:
    "click .destroy" : "destroy"

  constructor: (options) ->
    super(options)
    try
      @current_country = @model.get('country')
      @current_languages = @model.get('languages')
    catch error
      @current_country = null
      @current_languages = []

    return

  destroy: () ->
    this.remove()
    false

  render: ->
    $(@el).html(@template(countries: @collection))
    @fill_countries()
    @init_token_input()
    return this

  fill_countries: ->
    @elSelect = @.$el.find('.countries-selector')
    view = new PhenomenaTestTask.Views.Countries.OptionView(current_country: @current_country)
    @elSelect.html(view.render().el)
    return

  init_token_input: ->
    lang_input = @.$el.find('.languages')
    lang_input.tokenInput '/languages/search',
      preventDuplicates: true
      theme: 'facebook'
      prePopulate: name: lang for lang in @current_languages
    return
