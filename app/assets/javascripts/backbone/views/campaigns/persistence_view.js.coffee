PhenomenaTestTask.Views.Campaigns ||= {}

class PhenomenaTestTask.Views.Campaigns.PersistenceView extends Backbone.View

  fill_inputs: ->
    form = @.$el.find("form")
    trs = form.find('tr.country-row')
    countries = []

    trs.each (idx, tr) ->
      form_row = {}
      country_select = $(tr).find('select')
      form_row['country'] = country_select.val()

      form_row['languages'] = []
      $(tr).find('ul li p').each (idx, el) ->
        form_row['languages'].push $(el).html()

      countries.push form_row

    @model.set({countries_languages: countries})


  render : ->
    $(@el).html(@template(@model.toJSON() ))

    this.$("form").backboneLink(@model)

    _.defer(->
      $('.datepicker').datepicker
        format: 'dd-mm-yyyy'
      return
    )

    @add_rows()

    return this

  markErrorField: (name, error) ->
    field = @form.find(":input[name=" + name + "]")
    field.parent().append("<div class='help-block'>" + error + "</div>")
    field.parent().parent().addClass("error")

  cleanErrors: ->
    @form.find(".error").removeClass("error")
    @form.find(".help-block").remove()

  renderErrors: ->
    @errors = @model.get('errors')
    @form = this.$el.find("form")
    @cleanErrors()
    @markErrorField(name, error) for name, error of @errors if @errors?
    return this