class PhenomenaTestTask.Models.CountryLanguage extends Backbone.Model

  defaults:
    country: null
    language: null

class PhenomenaTestTask.Collections.CountriesLanguagesCollection extends Backbone.Collection
  model: PhenomenaTestTask.Models.CountryLanguage
