class PhenomenaTestTask.Models.Country extends Backbone.Model
  paramRoot: 'country'

  defaults:
    id: null
    name: null

class PhenomenaTestTask.Collections.CountriesCollection extends Backbone.Collection
  model: PhenomenaTestTask.Models.Country
  url: '/countries'
