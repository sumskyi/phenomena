require 'cucumber/websteps'

Given(/^a campaigns on file$/) do
  create(
    :campaign,
    :countries_languages => [{:country => 'Algeria', :languages => ['A', 'B']}]
  )
end

Given(/^countries on file$/) do
  create(:country, :short => 'AL', :name => 'Albania')
  create(:country, :short => 'UA', :name => 'Ukraine')
end

Given(/^languages on file$/) do
  create(:language, :name => 'Albanian')
  create(:language, :name => 'Ukrainian')
end

